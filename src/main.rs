#[macro_use]
extern crate lazy_static;
extern crate regex;

use std::fs::File;
use std::io::prelude::*;
use std::io::{BufReader, Result};

use regex::Regex;
use structopt::StructOpt;

#[derive(StructOpt)]
struct CLI {
    /// The path to the file to read
    #[structopt(parse(from_os_str))]
    path: std::path::PathBuf,
}

struct RegexHelperStruct {
    regex: &'static str,
    re: Regex,
}

trait RegexHelper {
    fn new(regex: &'static str) -> Self;
    fn regex(&self) -> &'static str;
    fn re(&self) -> &Regex;
    fn find(&self, text: &str);
    fn is_match(&self, text: &str) -> bool {
        self.re().is_match(text)
    }
}

impl RegexHelper for RegexHelperStruct {
    fn new(regex: &'static str) -> RegexHelperStruct {
        let re: Regex = Regex::new(regex).unwrap();
        RegexHelperStruct { regex, re }
    }

    fn regex(&self) -> &'static str {
        self.regex
    }

    fn re(&self) -> &Regex {
        &self.re
    }

    fn find(&self, text: &str) {
        if self.is_match(text) {
            let f = self.re().find(text).unwrap();
            println!("{}--{}", f.start(), f.end());
        } else {
            println!("pauses briefly...");
        }
    }
}

const ADJECTIVES: [&str; 3] = ["adj", "adjectif", "Adjectif"];
const ADVERBS: [&str; 2] = ["adv", "adverbe"];
const NOUNS: [&str; 1] = ["nom"];
const VERBS: [&str; 3] = ["verb", "verbe", "Verbe"];

fn check_cat(text: &str) -> bool {
    ADJECTIVES.contains(&text)
        || ADVERBS.contains(&text)
        || NOUNS.contains(&text)
        || VERBS.contains(&text)
}

struct Lemdef {
    id: i32,
    title: String,
    language: String,
    category: String,
    definition: String,
    specifications: Vec<String>,
    examples: Vec<String>,
    inflections: Vec<i32>,
    polysems: Vec<i32>,
    homonyms: Vec<i32>,
    synonyms: Vec<i32>,
    antonyms: Vec<i32>,
    hyponyms: Vec<i32>,
    hypernyms: Vec<i32>,
    metonyms: Vec<i32>,
    meronyms: Vec<i32>,
}

struct Inflection {
    id: i32,
    title: String,
    language: String,
    category: String,
    lemmas: Vec<i32>,
}

struct Definition {
    definition: String,
    specifications: Vec<String>,
    examples: Vec<String>,
    flexion: bool,
    verbs: Vec<String>,
}

struct Category {
    category: String,
    definitions: Vec<Definition>,
    synonyms: Vec<String>,
    hyponyms: Vec<String>,
    antonyms: Vec<String>,
    meronyms: Vec<String>,
}

struct Page {
    title: String,
    language: String,
    category: Vec<Category>,
}

enum RegexFn {
    IsMatch,
    Find,
    Captures,
}

enum RegexFnResult {
    Bool(bool),
    Text(String),
    Postion(usize, usize),
}

fn process(text: &str, regex_fn: &RegexFn, regex: &Regex) -> Option<RegexFnResult> {
    match regex_fn {
        RegexFn::IsMatch => Some(RegexFnResult::Bool(regex.is_match(text))),
        RegexFn::Find => regex
            .find(text)
            .map_or(None, |f| Some(RegexFnResult::Postion(f.start(), f.end()))),
        RegexFn::Captures => {
            let caps = regex.captures(text);
            match caps {
                Some(caps) => caps.get(1).map_or(None, |m| {
                    Some(RegexFnResult::Text(String::from(m.as_str())))
                }),
                None => None,
            }
        }
    }
}

fn title(text: &str, regex_fn: RegexFn) -> Option<RegexFnResult> {
    lazy_static! {
        static ref RE: Regex = Regex::new("<title>(.*?)</title>").unwrap();
    }
    process(&text, &regex_fn, &RE)
}

fn language(text: &str, regex_fn: RegexFn) -> Option<RegexFnResult> {
    lazy_static! {
        static ref RE: Regex = Regex::new("== \\{\\{langue\\|(fr)\\}\\} ==").unwrap();
    }
    process(&text, &regex_fn, &RE)
}

fn category(text: &str, regex_fn: RegexFn) -> Option<RegexFnResult> {
    lazy_static! {
        static ref RE: Regex = Regex::new("^\\s*={3} \\{\\{S\\|(.*)\\|fr\\}\\} ={3}\\s*$").unwrap();
    }
    process(&text, &regex_fn, &RE)
}

fn category_flexion(text: &str, regex_fn: RegexFn) -> Option<RegexFnResult> {
    lazy_static! {
        static ref RE: Regex =
            Regex::new("^\\s={3} \\{\\{S\\|(.*)\\|fr\\|flexion\\}\\} ={3}\\s*$").unwrap();
    }
    process(&text, &regex_fn, &RE)
}

fn print_val(text: &str) {
    if check_cat(text) {
        println!("{}", text);
    }
}

fn parse(pagevec: &Vec<String>) {
    println!("{}", pagevec[2]);
    for line in pagevec.iter() {
        let val = title(&line, RegexFn::Captures);
        if let Some(val) = val {
            if let RegexFnResult::Text(val) = val {
                println!("{}", val);
            }
        }
        let val = language(&line, RegexFn::Captures);
        if let Some(val) = val {
            if let RegexFnResult::Text(val) = val {
                println!("{}", val);
            }
        }
        let val = category(&line, RegexFn::Captures);
        if let Some(val) = val {
            if let RegexFnResult::Text(val) = val {
                print_val(&val);
            }
        }
        let val = category_flexion(&line, RegexFn::Captures);
        if let Some(val) = val {
            if let RegexFnResult::Text(val) = val {
                print_val(&val);
            }
        }
    }
}

fn check_validity_page(pagevec: &Vec<String>) -> bool {
    !pagevec[1].contains(":")
}

fn check_validity_parsing(text: &String) -> bool {
    if text.is_empty() {
        return false;
    }
    if text.trim_start().starts_with("<page>")
        || text.trim_start().starts_with("</page>")
        || text.trim_start().starts_with("<title>")
        || text.trim_start().starts_with("*")
        || text.trim_start().starts_with("=")
        || text.trim_start().starts_with("'")
        || text.trim_start().starts_with("#")
    {
        return true;
    }
    // println!("{}", text);
    false
}

fn main() -> Result<()> {
    let args = CLI::from_args();
    let file = File::open(&args.path)?;
    let file = BufReader::new(file);

    let mut pagevec: Vec<String> = Vec::new();
    let mut append: bool = false;

    for line in file.lines() {
        let l: String = line.unwrap();

        if append && l.contains("</page>") {
            append = false;
            // don't wanna parse specific pages
            if !pagevec[1].contains(":") {
                parse(&pagevec);
            }
            pagevec.clear();
            println!("==========================");
        }

        if !append && l.contains("<page>") {
            append = true;
        }

        if append && check_validity_parsing(&l) {
            pagevec.push(l);
        }
    }

    Ok(())
}
